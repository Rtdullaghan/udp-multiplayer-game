#include <KarnageServerPCH.h>

bool Server::StaticInit()
{
	ConnectionDetails::StaticInit();
	sInstance.reset(new Server());
	return true;
}

Server::Server() :
	mPickupSpawnTimer(Constant::World::Pickup_Spawn_Time)
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('PLYR', PlayerServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('PKUP', PickupServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULT', BulletServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('GRDE', GrenadeServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('OBST', ObstacleServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLUD', BloodstainServer::StaticCreate);
	InitNetworkManager(); 
	
	m_spawnPoints = {
		sf::Vector2f(128, 128), 
		sf::Vector2f(1408, 128), 
		sf::Vector2f(1408, 1280), 
		sf::Vector2f(128, 1280), //TODO MAGIC NUMBERS
		sf::Vector2f(768, 128), 
		sf::Vector2f(1408, 640), 
		sf::Vector2f(768, 1280), 
		sf::Vector2f(128, 640),
		sf::Vector2f(768, 640) 
	};

	mPickupSpawnTimer.Start();
}

int Server::Run()
{
	SetupWorld();
	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	uint16_t port = ConnectionDetails::sInstance->GetServerPort();
	return NetworkManagerServer::StaticInit(port);
}

namespace
{
	void CreateRandomPickup() 
	{
			shared_ptr<Pickup> pickup =
			std::static_pointer_cast<Pickup>(GameObjectRegistry::sInstance->CreateGameObject('PKUP'));

			pickup->SetLocation(sf::Vector2f(Utility::GetRandomVector(sf::Vector2i(Constant::Pickup::MAX_X_WORLD_BOUNDARIES, Constant::Pickup::MAX_Y_WORLD_BOUNDARIES))));
			pickup->SetPickupType(Utility::randomInt(Enum::PickupType::TYPE_COUNT));
	}

	void CreateRandomObstacle()
	{
		shared_ptr<Obstacle> obstacle =
			std::static_pointer_cast<Obstacle>(GameObjectRegistry::sInstance->CreateGameObject('OBST'));
		
		obstacle->SetLocation(sf::Vector2f(Utility::GetRandomVector(sf::Vector2i(Constant::Obstacle::MAX_X_WORLD_BOUNDARIES, Constant::Obstacle::MAX_Y_WORLD_BOUNDARIES))));
		obstacle->SetObstacleType(Utility::randomInt(Enum::ObstacleType::OBSTACLE_COUNT));
	}
}

void Server::PickupUpdate()
{
	mPickupSpawnTimer.Update(Timing::sInstance.GetDeltaTime());

	if (mPickupSpawnTimer.IsFinished())
	{
		mPickupSpawnTimer.Reset();
		mPickupSpawnTimer.Start();
		CreateRandomPickup();
	}
}

void Server::SetupWorld()
{
	/* Create Obstacles */
	for (size_t i = 0; i < Constant::Obstacle::OBSTACLE_SPAWN_COUNT; i++)
	{
		CreateRandomObstacle();
	}
}

void Server::DoFrame()
{

	NetworkManagerServer::sInstance->ProcessIncomingPackets();
	NetworkManagerServer::sInstance->CheckForDisconnects();
	NetworkManagerServer::sInstance->RespawnPlayers();
	Engine::DoFrame();
	NetworkManagerServer::sInstance->SendOutgoingPackets();
	this->PickupUpdate();
}

void Server::HandleNewClient(ClientProxyPtr inClientProxy)
{
	int playerId = inClientProxy->GetPlayerId();
	string playerName = inClientProxy->GetName();
	ScoreBoardManager::sInstance->AddEntry(playerId, playerName);
	SpawnPlayerForPlayer(playerId, playerName);
}

void Server::SpawnPlayerForPlayer(int inPlayerId, string inPlayerName)
{
	PlayerPtr player = std::static_pointer_cast<Player>(GameObjectRegistry::sInstance->CreateGameObject('PLYR'));
	player->SetPlayerId(inPlayerId); // Pick one of a few random locations.
	player->SetPlayerName(inPlayerName);

	int randomIndex = inPlayerId % m_spawnPoints.size();
	player->SetLocation(m_spawnPoints[randomIndex]);
}

void Server::HandleLostClient(ClientProxyPtr inClientProxy) const
{
	//kill client's player
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();
	ScoreBoardManager::sInstance->RemoveEntry(playerId);
	PlayerPtr player = GetPlayerForPlayer(playerId);
	if(player)
	{
		player->SetDoesWantToDie(true);
	}
}

PlayerPtr Server::GetPlayerForPlayer(int inPlayerId) const
{
	//run through the objects till we find the player...
	//it would be nice if we kept a pointer to the player on the clientproxy
	//but then we'd have to clean it up when the player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for(auto go : gameObjects)
	{
		Player* player = go->GetAsPlayer();
		if(player && player->GetPlayerId() == inPlayerId)
		{
			return std::static_pointer_cast<Player>(go);
		}
	}
	return nullptr;
}
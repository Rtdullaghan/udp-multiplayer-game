#include <KarnageServerPCH.h>

ObstacleServer::ObstacleServer() {}

void ObstacleServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

bool ObstacleServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	/* Do collisions */
	return true;
}

bool ObstacleServer::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	inObj->SetDoesWantToDie(true);
	return false;
}
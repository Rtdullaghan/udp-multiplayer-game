#include <KarnageServerPCH.h>
#include <iostream>

BloodstainServer::BloodstainServer() {}

void BloodstainServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}
bool BloodstainServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	/* Do collisions */
	return false;
}

bool BloodstainServer::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	inObj->SetDoesWantToDie(false);
	return false;
}
#pragma once
class Server : public Engine{
public:
	static bool StaticInit();
	void DoFrame() override;
	int Run() override;
	void HandleNewClient(ClientProxyPtr inClientProxy);
	void SpawnPlayerForPlayer(int inPlayerId, string inPlayerName);
	void HandleLostClient(ClientProxyPtr inClientProxy) const;
	PlayerPtr GetPlayerForPlayer(int inPlayerId) const;
private:
	Server();
	//uint8_t TimeBetweenSpawns;
	//uint8_t SpawnTime;

	Timer mPickupSpawnTimer;

	static bool InitNetworkManager();
	void PickupUpdate();
	static void SetupWorld();
	std::vector<sf::Vector2f> m_spawnPoints;
};

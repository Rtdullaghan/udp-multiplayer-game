#pragma once
enum EPlayerControlType{
	ESCT_Human,
	ESCT_AI
};

class PlayerServer : public Player{
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new PlayerServer());
	}

	void HandleDying() override;
	void Update() override;

	void SetPlayerControlType(EPlayerControlType inPlayerControlType)
	{
		mPlayerControlType = inPlayerControlType;
	}

	void TakeDamage(uint8_t damageAmount, uint32_t playerId);
	void IncScore(uint32_t playerId);
	void KillPlayer(uint32_t playerId);
protected:
	PlayerServer();
private:
	void SetOldLocation();
	void UpdateMoves();
	void UpdateLocation() const;
	void ProcessMoveList(ClientProxyPtr client);

	void HandleShooting();
	void CreateBloodstain();
	void HandleGrenades();
	void HandlePickups();
	void HandleReadyToDie();
	void HandlePickupTimer();
	void StartPickupTimer();
	void UpdatePickupTimer();
	void HandleEvents();
	void HandleLavaCollision();

private:
	
	EPlayerControlType mPlayerControlType;

	float mTimeOfNextShot;
	float mTimeOfNextLavaDamage;
	float mTimeBetweenLavaDamage;
	float oldRotation{};


	sf::FloatRect mWorldBounds;
	sf::Vector2f oldLocation;
	sf::Vector2f oldVelocity;
	
	Timer mPickupTimer;
	Timer mDeathAnimationTimer;

	bool mPickupTimerStarted{};

	bool mReadyToThrow{};
	bool mThrowGrenade{};

	Timer MGrenadeTimer;

};

#pragma once
class PickupServer : public Pickup{
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new PickupServer());
	}

	void HandleDying() override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

protected:
	PickupServer();

private:
	void ApplyPickup(Player* inPlayer);
	void ApplyHealthPickup(Player* inPlayer);
	void ApplyArmourPickup(Player* inPlayer);
	void ApplyCloakPickup(Player* inPlayer);
	void ApplyAmmoPickup(Player* inPlayer);
	void ApplyFireRatePickup(Player* inPlayer);
	void ApplyShotgunPickup(Player* inPlayer);
};

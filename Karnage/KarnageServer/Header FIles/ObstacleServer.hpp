#pragma once
class ObstacleServer : public Obstacle {
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new ObstacleServer());
	}

	void HandleDying() override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

protected:
	ObstacleServer();
};
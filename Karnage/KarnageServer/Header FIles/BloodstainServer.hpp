#pragma once
class BloodstainServer : public Bloodstain {
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new BloodstainServer());
	}

	void HandleDying() override;

	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

protected:
	BloodstainServer();
};
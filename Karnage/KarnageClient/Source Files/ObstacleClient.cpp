#include <KarnageClientPCH.h>

void ObstacleClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite->GetSprite(), states);
}

bool ObstacleClient::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	return false;
}

ObstacleClient::ObstacleClient()
{
	m_sprite.reset(new SFSpriteComponent(this));
	m_sprite->SetTexture(SFTextureManager::sInstance->GetTexture("obstacles"));
}

void ObstacleClient::SetObstacleType(uint8_t type)
{
	mObstacleType = static_cast<Enum::ObstacleType>(type);
	SetTextureRect();
	m_sprite->CenterOrigin();
}

void ObstacleClient::SetTextureRect() const
{
	m_sprite->CenterOrigin();
	switch (mObstacleType)
	{
	case Enum::ObstacleType::CRATE:
		m_sprite->SetTextureRect(sf::IntRect(0, 0, 128, 128));
		break;
	default:
		break;
	}
}

#pragma region Networking

void ObstacleClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	ReadObstaclePosition(inInputStream, stateBit);

	ReadObstacleType(inInputStream, stateBit);
}

void ObstacleClient::ReadObstaclePosition(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		sf::Vector2f location;
		inInputStream.Read(location.x);
		inInputStream.Read(location.y);
		SetLocation(location);
		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
}

void ObstacleClient::ReadObstacleType(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint8_t ObstacleType;
		inInputStream.Read(ObstacleType, Constant::Networking::OBSTACLE_TYPE_BITS);
		SetObstacleType(ObstacleType);
	}
}

#pragma endregion
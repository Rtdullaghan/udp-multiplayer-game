#include <KarnageClientPCH.h>

void BloodstainClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite->GetSprite(), states);
}

BloodstainClient::BloodstainClient()
{
	m_sprite.reset(new SFSpriteComponent(this));
	m_sprite->SetTexture(SFTextureManager::sInstance->GetTexture("playerDeathAnimation"));
}

void BloodstainClient::SetBloodstainType(uint8_t type)
{
	mBloodstainType = static_cast<Enum::BloodstainType>(type);
	SetTextureRect();
	m_sprite->CenterOrigin();
}

void BloodstainClient::SetTextureRect() const
{
	m_sprite->CenterOrigin();
	switch (mBloodstainType)
	{
	case Enum::BloodstainType::SPLATTER_ONE:
		m_sprite->SetTextureRect(sf::IntRect(275, 0, 275, 217));
		break;
	case Enum::BloodstainType::SPLATTER_TWO:
		m_sprite->SetTextureRect(sf::IntRect(550, 0, 275, 128));
		break;
	case Enum::BloodstainType::SPLATTER_THREE:
		m_sprite->SetTextureRect(sf::IntRect(825, 0, 275, 128));
		break;
	default:
		break;
	}
}

#pragma region Networking

void BloodstainClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	ReadBloodstainPosition(inInputStream, stateBit);

	ReadBloodstainType(inInputStream, stateBit);
}

void BloodstainClient::ReadBloodstainPosition(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		sf::Vector2f location;
		inInputStream.Read(location.x);
		inInputStream.Read(location.y);
		SetLocation(location);
		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
}

void BloodstainClient::ReadBloodstainType(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint8_t BloodstainType;
		inInputStream.Read(BloodstainType, Constant::Networking::BLOODSTAIN_TYPE_BITS);
		SetBloodstainType(BloodstainType);
	}
}

#pragma endregion

#pragma once
class ReplicationManagerClient{
public:
	void Read(InputMemoryBitStream& inInputStream) const;
private:
	void ReadAndDoCreateAction(InputMemoryBitStream& inInputStream, int inNetworkId) const;
	static void ReadAndDoUpdateAction(InputMemoryBitStream& inInputStream, int inNetworkId);
	static void ReadAndDoDestroyAction(InputMemoryBitStream& inInputStream, int inNetworkId);
	static void ReadAndDoRPCAction(InputMemoryBitStream& inInputStream, int inNetworkId);
};

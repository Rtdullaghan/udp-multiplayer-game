#pragma once
//I take care of rendering things!
class SFRenderManager{
public:
	enum ERenderState {
		START_SCREEN,
		GAME_SCREEN,
		WINNER_SCREEN,
		DEATH_SCREEN
	};

	static std::unique_ptr<SFRenderManager> sInstance;

	static void StaticInit();

	void Load();
	void Render();

	sf::Vector2f FindPlayerCentre() const;

	void DestroyLocalPlayer(uint32_t playerID);

private:
	SFRenderManager();
	

	void SetAlivePlayers();
	void RenderComponents();
	void SetRenderState();
	void SetTextures();
	void Update();
	void UpdateLocalPlayer();
	void DrawWorld();
	void RenderUI() const;
	void UpdateView();
	void RenderTexturedWorld() const;
	void CountFPS();


	bool IsLastManStanding() const;
	bool GameStarted() const;

	bool LocalPlayerAlive();

	uint8_t FindPlayerHealth() const;
	void SetLocalPlayer(uint32_t id);
	
private:
	sf::View view;
	sf::View HUD;

	sf::Sprite m_startScreen;
	sf::Sprite m_diedScreen;
	sf::Sprite m_winnerScreen;

	sf::Vector2f m_lastPlayerPos;
	sf::Vector2f textPos;
	sf::Vector2f mViewSize;
	sf::Vector2f mViewCenter;

	sf::Vector2f mArrowPosition;

	TileFactory mTileFactory;


	Player* mLocalPlayer;

	ERenderState mRenderState;
	uint8_t mAlivePlayers;

	sf::Clock mFrameClock;

	unsigned int mFrame;
	unsigned int mFPS;
	unsigned int mDrawnObjects;

	bool mDebug;
	bool mLocalPlayerAlive;

	bool mGameReady;
	bool mGameStarted;
	bool mGameOver;

	uint32_t mLocalPlayerID;
};

#pragma once
class Client : public Engine{
public:
	static bool StaticInit();
protected:
	Client();
	void DoFrame() override;
	void HandleEvent(sf::Event& p_event) override;
	bool PullEvent(sf::Event& p_event) override;
};

#pragma once
//Mike
class TileFactory{
public: //TODO Add more Maps 
	enum class MapID{
		Dessert,
		City,
		Forrest,
		TypeCount
	};

public:
	TileFactory();
	void populateMap();
	void populateLine();
	sf::FloatRect getMapBounds() const;
	sf::Vector2u getTileSize() const;
	sf::VertexArray getVertices() const;
	//void SetVeticiesInView(sf::FloatRect view);
	//sf::VertexArray getVerticesInView() const;

	sf::Texture getTexture() const;
	static void findCharacter(unsigned int& i, std::string line);
private:
	void setMapDimensions(unsigned int width);
	bool loadMap();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
private:
	std::ifstream mMapFile;
	sf::Texture mTexture;
	std::vector<int> tempMap;
	std::vector<int> intMap;
	sf::Vector2u mTileSize;
	sf::VertexArray m_vertices;

	sf::VertexArray mViewVertices;

	unsigned int mMapWidth;
	unsigned int mMapHeight;
};

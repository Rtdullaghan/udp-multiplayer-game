#pragma once
class NetworkManagerClient : public NetworkManager{
	enum NetworkClientState{
		NCS_Uninitialized,
		NCS_SayingHello,
		NCS_Welcomed
	};

public:
	static NetworkManagerClient* sInstance;
	static void StaticInit(const SocketAddress& inServerAddress, const string& inName);
	void SendOutgoingPackets();
	void ProcessPacket(InputMemoryBitStream& inInputStream, const SocketAddress& inFromAddress) override;

	const WeightedTimedMovingAverage& GetAvgRoundTripTime() const
	{
		return mAvgRoundTripTime;
	}

	float GetRoundTripTime() const
	{
		return mAvgRoundTripTime.GetValue();
	}

	uint32_t GetPlayerId() const
	{
		return mPlayerId;
	}

	float GetLastMoveProcessedByServerTimestamp() const
	{
		return mLastMoveProcessedByServerTimestamp;
	}

private:
	NetworkManagerClient();
	void Init(const SocketAddress& inServerAddress, const string& inName);
	void UpdateSayingHello();
	void SendHelloPacket();
	void HandleWelcomePacket(InputMemoryBitStream& inInputStream);
	void HandleStatePacket(InputMemoryBitStream& inInputStream);
	void ReadLastMoveProcessedOnServerTimestamp(InputMemoryBitStream& inInputStream);
	void HandleGameObjectState(InputMemoryBitStream& inInputStream);
	static void HandleScoreBoardState(InputMemoryBitStream& inInputStream);
	void UpdateSendingInputPacket();
	void SendInputPacket();
	void DestroyGameObjectsInMap(const IntToGameObjectMap& inObjectsToDestroy);
	DeliveryNotificationManager mDeliveryNotificationManager;
	ReplicationManagerClient mReplicationManagerClient;
	SocketAddress mServerAddress;
	NetworkClientState mState;
	float mTimeOfLastHello;
	float mTimeOfLastInputPacket;
	string mName;
	uint32_t mPlayerId;
	float mLastMoveProcessedByServerTimestamp;
	WeightedTimedMovingAverage mAvgRoundTripTime;
	float mLastRoundTripTime;
};

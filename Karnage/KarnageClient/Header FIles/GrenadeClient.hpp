#pragma once
class GrenadeClient : public Grenade {
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new GrenadeClient());
	}

	void Update() override;

	void Read(InputMemoryBitStream& inInputStream) override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;
	
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
protected:
	GrenadeClient();

private:
	void ReadLocation(InputMemoryBitStream& inInputStream, bool& stateBit);
	void ReadPlayerID(InputMemoryBitStream& inInputStream, bool& stateBit);
	void ReadExploding(InputMemoryBitStream& inInputStream, bool& stateBit);

	void SetAnimations();
	void Explode();
private:
	SFSpriteComponentPtr m_sprite;

	Animation mExplosionAnimation;
};

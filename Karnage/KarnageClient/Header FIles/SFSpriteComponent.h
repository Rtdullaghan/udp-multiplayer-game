#pragma once
typedef shared_ptr<sf::Texture> SFTexturePtr;
typedef shared_ptr<sf::Font> FontPtr;

class SFSpriteComponent{
public:
	SFSpriteComponent(GameObject* inGameObject);
	virtual ~SFSpriteComponent();

	void SetTexture(SFTexturePtr inTexture) const;
	void SetTextureRect(sf::IntRect textureRect) const;
	virtual sf::Sprite& GetSprite();
	void SetSprite(sf::Sprite& sprite);
	void SetAnimation(Animation& animation);
	void UpdateAnimation(float dt);
	void SetColor(sf::Color color) const;
	void CenterOrigin() const;
	sf::Vector2f GetLocation() const;



	//void SetAsPlayer(uint32_t id, string name);
	//bool isPlayerSprite(uint32_t playerID);

	//PlayerArrow* GetPlayerArrow();
protected:
	sf::Sprite* m_sprite; //don't want circular reference...
	GameObject* mGameObject;

private:
	Animation mAnimation;

	PlayerArrow* mPlayerArrow;
	bool mIsPlayer;
	uint32_t mID;
};

typedef shared_ptr<SFSpriteComponent> SFSpriteComponentPtr;


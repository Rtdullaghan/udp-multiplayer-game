class Bullet : public GameObject {
public:
	CLASS_IDENTIFICATION('BULT', GameObject)

	enum EBulletReplicationState{
		EYRS_Pose = 1 << 0,
		EYRS_PlayerId = 1 << 2,
		EYRS_AllState = EYRS_Pose | EYRS_PlayerId
	};

	static GameObject* StaticCreate()
	{
		return new Bullet();
	}

	uint32_t GetAllStateMask() const override
	{
		return EYRS_AllState;
	}

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	void SetVelocity(const sf::Vector2f& inVelocity)
	{
		mVelocity = inVelocity;
	}

	const sf::Vector2f& GetVelocity() const
	{
		return mVelocity;
	}

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}

	uint32_t GetPlayerId() const
	{
		return mPlayerId;
	}

	void InitFromShooter(Player* inShooter, int variation);
	void Update() override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	void ProcessBulletCollisions();
protected:
	Bullet();
	sf::Vector2f mVelocity;
	float mMuzzleSpeed;
	uint32_t mPlayerId;
};

typedef shared_ptr<Bullet> BulletPtr;
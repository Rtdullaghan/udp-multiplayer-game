class ScoreBoardManager{
public:
	static void StaticInit();
	static std::unique_ptr<ScoreBoardManager> sInstance;

	class Entry{
	public:
		Entry():
		       mPlayerId(0)
		       , mScore(0) { } ;

		Entry(uint32_t inPlayerID, const string& inPlayerName);

		uint32_t GetPlayerId() const
		{
			return mPlayerId;
		}

		const string& GetPlayerName() const
		{
			return mPlayerName;
		}

		const string& GetFormattedNameScore() const
		{
			return mFormattedNameScore;
		}

		int GetScore() const
		{
			return mScore;
		}

		void SetScore(uint8_t inScore);
		bool Write(OutputMemoryBitStream& inOutputStream) const;
		bool Read(InputMemoryBitStream& inInputStream);

	private:
		uint32_t mPlayerId;
		string mPlayerName;
		uint8_t mScore;
		string mFormattedNameScore;
	};

	Entry* GetEntry(uint32_t inPlayerId);
	bool RemoveEntry(uint32_t inPlayerId);
	void AddEntry(uint32_t inPlayerId, const string& inPlayerName);
	uint16_t GetHighScore();
	void IncScore(uint32_t inPlayerId, int inAmount);
	bool Write(OutputMemoryBitStream& inOutputStream) const;
	bool Read(InputMemoryBitStream& inInputStream);

	const vector<Entry>& GetEntries() const
	{
		return mEntries;
	}

private:
	ScoreBoardManager();
	vector<Entry> mEntries;

	uint16_t mHighScore;
};

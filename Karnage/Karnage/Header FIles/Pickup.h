class Pickup : public GameObject{
public:
	CLASS_IDENTIFICATION('PKUP', GameObject)

	static GameObject* StaticCreate()
	{
		return new Pickup();
	}

	uint32_t GetAllStateMask() const override
	{
		return  PickupReplication::State::AllState;
	}

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	void WritePickupPosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;
	void WritePickupType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;

	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	void SetPickupType(uint8_t type);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	Pickup();
	bool picked;
	Enum::PickupType mPickUpType;
};

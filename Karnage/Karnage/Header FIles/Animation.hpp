class Animation : public sf::Drawable, public sf::Transformable{
public:
	Animation();
	explicit Animation(const sf::Texture& texture);
	void setTexture(const sf::Texture& texture);
	const sf::Texture* getTexture() const;
	void CenterOrigin();
	void setFrameSize(sf::Vector2i mFrameSize);
	void setFrameSize(sf::IntRect frameSize);
	sf::Vector2i getFrameSize() const;
	void setNumFrames(std::size_t numFrames);
	std::size_t getNumFrames() const;
	void setDuration(float duration);
	float getDuration() const;
	void setRepeating(bool flag);
	bool isRepeating() const;
	void restart();
	bool isFinished() const;
	sf::FloatRect getLocalBounds() const;
	sf::FloatRect getGlobalBounds() const;
	void update(float dt);
	void setTextureRect();
	sf::Sprite* getSprite();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:
	sf::Sprite mSprite;
	sf::Vector2i mFrameSize;
	std::size_t mNumFrames;
	std::size_t mCurrentFrame;
	float mDuration;
	float mElapsedTime;
	bool mRepeat;
};
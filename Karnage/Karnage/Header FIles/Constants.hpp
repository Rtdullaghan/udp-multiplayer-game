#pragma once

namespace Constant
{
	namespace Window
	{
		static const short		VIEW_WIDTH = 800;
		static const short		VIEW_HEIGHT = 600;
		static const float		CAMERA_FOLLOW_RATE = .01f;
		static const string		WINDOW_TITLE = "Karnage";
	}

	namespace Font
	{
		static const uint8_t		FONT_MEDIUM = 24;
		static const uint8_t		FONT_LARGE = 36;
	}

	namespace GameObject
	{
		static const uint32_t		PLAYER = static_cast<uint32_t>('PLYR');
	}

	namespace Player
	{
		static const uint8_t		MAX_HEALTH = 100;
		static const uint8_t		MAX_ARMOUR = 50;
		static const uint8_t		MIN_ARMOUR = 0;

		static const uint16_t		SPEED = 150;
		static const uint8_t		ROTATION_SPEED = 5;

		static const float			FIRE_RATE = 0.2f;
		static const uint8_t		GUN_KICKBACK = 8;
		static const float			RESPAWN_DELAY = 3.f;

		static const sf::Vector2f	PLAYER_ARROW_POSITION(375, 250);
	}

	namespace World
	{
		static const uint8_t		LAVA_DAMAGE = 10;
		static const uint8_t		BULLET_DAMAGE = 10;
		static const float			BULLET_LIFETIME = 1.5f;

		static const uint8_t		GRENADE_DAMAGE = 50;
		static const float			GRENADE_LIFETIME = 3.f;
		static const float			GRENADE_THROW_SPEED = 200.f;

		static const float			GRENADE_MAX_DISTANCE = 1000.f;

		static const uint8_t		EXPLOSION_RADIUS = 50;

		static const uint8_t		Pickup_Spawn_Time = 10;

		static const uint8_t		MAX_NAME_SIZE = 6;
	}

	namespace Pickup
	{
		static const uint8_t		PICKUP_TIMER = 10; //Seconds
		static const uint8_t		PICKUP_HEALTH = 10;
		static const uint8_t		PICKUP_ARMOUR = 10;
		static const uint8_t		PICKUP_AMMO = 3;
		static const float			FIRE_RATE_MULTIPLIER = 0.1f;
		static const uint16_t		MAX_X_WORLD_BOUNDARIES = 1536;
		static const uint16_t		MAX_Y_WORLD_BOUNDARIES = 1408;
		
	}

	namespace Obstacle
	{
		static const uint8_t		OBSTACLE_HEALTH = 100;
		static const uint8_t		OBSTACLE_SPAWN_COUNT = 15;
		static const uint16_t		MAX_X_WORLD_BOUNDARIES = 1536;
		static const uint16_t		MAX_Y_WORLD_BOUNDARIES = 1408;
	}

	namespace Animation
	{
		static const sf::IntRect	PLAYER_MOVE_FRAME_SIZE = sf::IntRect(0, 0, 79, 132);
		static const uint8_t		PLAYER_MOVE_FRAMES = 20;
		static const uint8_t		PLAYER_MOVE_DURATION = 1;

		static const sf::IntRect	PLAYER_DEATH_FRAME_SIZE = sf::IntRect(0, 0, 275, 217);
		static const uint8_t		PLAYER_DEATH_FRAMES = 6;
		static const uint8_t		PLAYER_DEATH_DURATION = 1;

		static const sf::IntRect	EXPLOSION_FRAME_SIZE = sf::IntRect(0, 0, 256, 256);
		static const uint8_t		EXPLOSION_FRAMES = 16;
		static const uint8_t		EXPLOSION_DURATION = 1;
	}

	namespace Networking
	{
		static const uint8_t		PLAYER_ID_BITS = 8; //Extra bits to account for players quiting and joining again
		static const uint8_t		HEALTH_BITS = 7;
		static const uint8_t		ARMOUR_BITS = 6;
		static const uint8_t		AMMO_BITS = 2;
		static const uint8_t		PICKUP_TYPE_BITS = 3;
		static const uint8_t		OBSTACLE_TYPE_BITS = 2;
		static const uint8_t		BLOODSTAIN_TYPE_BITS = 2;
		static const uint8_t		SCORE_BITS = 4;

		static const uint32_t		HELLO_CODE = 'HELO';
		static const uint32_t		WELCOME_CODE = 'WLCM';
		static const uint32_t		STATE_CODE = 'STAT';
		static const uint32_t		INPUT_CODE = 'INPT';

		static const int			MAX_PACKETS_PER_FRAME_COUNT = 10;

		static const float			TIME_BETWEEN_INPUT_SAMPLES = 0.003f;
		static const float			TIME_BETWEEN_INPUT_PACKETS = 0.003f;
		static const float			TIME_BETWEEN_HELLOS = 1.f;
	}

	namespace Score
	{
		static const uint8_t		WINNING_SCORE_AMOUNT = 10;

		static const sf::Vector2f	SCORE_POSITION(340.f, 50.f);
		static const sf::Color		SCORE_FILL_COLOR(sf::Color::White);
		static const sf::Color		SCORE_OUTLINE_COLOR(sf::Color::Black);
		static const float			SCORE_OUTLINE_THICKNESS = 0.3f;

		static const sf::Vector2f	SCOREBOARD_POSITION(300.f, 250.f);
		static const sf::Color		SCOREBOARD_FILL_COLOR(sf::Color::White);
		static const sf::Color		SCOREBOARD_OUTLINE_COLOR(sf::Color::Black);
		static const float			SCOREBOARD_OUTLINE_THICKNESS = 0.3f;
	}

	namespace HUD
	{
		namespace Debug
		{
			namespace Positions
			{
				static const sf::Vector2f	RTT_POSITION(10, 20);
				static const sf::Vector2f	INOUT_POSITION(130, 20);
				static const sf::Vector2f	DRAWN_OBJECTS_POSITION(10, 50);
				static const sf::Vector2f	SCOREBOARD_POSITION(400, 20);
				static const sf::Vector2f	FPS_POSITION(130, 50);
			}

			namespace Colors
			{
				static const sf::Color		RTT_FILL_COLOR(sf::Color::Black);
				static const sf::Color		INOUT_FILL_COLOR(sf::Color::Black);
				static const sf::Color		D_OBJECTS_FILL_COLOR(sf::Color::Black);
				static const sf::Color		FPS_FILL_COLOR(sf::Color::Black);
				static const sf::Color		SCOREBOARD_FILL_COLOR(sf::Color::Black);

				static const sf::Color		RTT_OUTLINE_COLOR(sf::Color::Red);
				static const sf::Color		INOUT_OUTLINE_COLOR(sf::Color::Red);
				static const sf::Color		D_OBJECTS_OUTLINE_COLOR(sf::Color::Red);
				static const sf::Color		FPS_OUTLINE_COLOR(sf::Color::Red);
				static const sf::Color		SCOREBOARD_OUTLINE_COLOR(sf::Color::Red);
			}

			static const float			OUTLINE_THICKNESS = 0.5f;
		}

		namespace Grenade
		{
			static const uint8_t		GRENADE_OFFSET = 40;
			static const float			GRENADE_SCALE = 0.05f;
		}
		
		namespace Player
		{
			static const uint8_t	HEALTH_BAR_LOW_THRESHOLD = 20;
			static const uint8_t	HEALTH_BAR_MEDIUM_THRESHOLD = 60;

			static const sf::Color	HEALTH_BAR_LOW_COLOR = sf::Color::Red;
			static const sf::Color	HEALTH_BAR_MEDIUM_COLOR = sf::Color::Yellow;
			static const sf::Color	HEALTH_BAR_HIGH_COLOR = sf::Color::Green;
		}
	}

	namespace Sound
	{
		static const uint16_t      SHOOT_VOLUME = 4000;
	}
}

namespace Enum
{
	enum PickupType {
		HEALTH,
		CLOAK,
		AMMO,
		FIRE_RATE,
		SHOTGUN,
		ARMOUR,
		TYPE_COUNT,
		NONE
	};

	enum ObstacleType {
		CRATE,
		OBSTACLE_COUNT
	};

	enum BloodstainType{
	    SPLATTER_ONE,
	    SPLATTER_TWO,
	    SPLATTER_THREE,
		BLOODSTAIN_COUNT
	};

	enum Sound { //TODO CHECK IF THESE NEED TO BE BITSHIFTED
		P_HEALTH,
		P_ARMOUR,
		P_AMMO,
		P_CLOAK,
		P_FIRE_RATE,
		P_SHOTGUN,
		SHOOT,
		DEATH,
		JOIN,
		EXPLOSION 
	};
}
#pragma
class Bloodstain : public GameObject {
public:
	CLASS_IDENTIFICATION('BLUD', GameObject)


		static GameObject* StaticCreate()
	{
		return new Bloodstain();
	}

	uint32_t GetAllStateMask() const override
	{
		return  BloodstainReplication::State::AllState;
	}

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	void WriteBloodstainPosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;
	void WriteBloodstainType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;

	void SetBloodstainType(uint8_t type);

	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	Bloodstain();
	Enum::BloodstainType mBloodstainType;
};


class Animation;

class Player : public GameObject{
public:
	CLASS_IDENTIFICATION('PLYR', GameObject)
	static GameObject* StaticCreate();

	uint32_t GetAllStateMask() const override;
	Player* GetAsPlayer() override;
	float GetFireRate() const;
	uint32_t GetPlayerId() const;
	string GetPlayerName() const;
	HUD* GetHUD() const;
	PlayerArrow* GetPlayerArrow() const;
	const sf::Vector2f& GetVelocity() const;
	const Enum::PickupType& GetPickupType() const;
	bool GetShotgun();
	uint8_t& GetHealth();
	uint8_t& GetArmour();
	uint8_t& GetGrenadeAmmo();

	void SetFlash();
	void SetLocal();
	void SetPlayerId(uint32_t inPlayerId);
	void SetPlayerName(string name);
	void SetVelocity(const sf::Vector2f& inVelocity);
	void SetGrenadeAmmo(uint8_t ammo);
	void SetPickupType(Enum::PickupType type);
	void SetReadyToDie(bool ready);

	void Update() override;
	void UpdateHUDAmmo() const;
	void UpdateHUDName() const;
	void UpdateHUDHealth() const;
	void UpdateHUDArmour() const;

	bool IsMoving() const;
	bool IsLocal() const;

	void ProcessInput(float inDeltaTime, const InputState& inInputState);
	void SimulateMovement(float inDeltaTime);

	void ApplyKickBack(float inDeltaTime);
	void ProcessCollisions();
	void PickupHealth(uint8_t health);
	void PickupArmour(uint8_t armour);

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

private:
	void WritePlayerID(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerPosition(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerThrust(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerHealth(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerAmmo(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerPickup(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerArmour(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;

protected:
	Player();
	void ApplyPickup();

private:
	void AdjustVelocityByThrust(float inDeltaTime);

protected:
	Enum::PickupType mPickupType;

	sf::FloatRect mWorldBounds;

	sf::Vector2f oldLocation;
	sf::Vector2f oldVelocity;
	sf::Vector2i mThrustDir;

	float mTimeBetweenShots;
	float mLastMoveTimestamp;
	float oldRotation;
	float mFlashTimer;


	uint8_t mHealth;
	uint8_t mArmour;
	uint8_t mGrenadeAmmo;

	bool mIsShooting;
	bool mIsThrowingGrenade;


	bool flash;
	bool mFlashTimerStarted;
	bool mReadyToDie;

	bool mIsCloaked;
	bool mEquipedShotgun;


	PlayerArrow* mPlayerArrow;

private:
	HUD* mHUD;
	
	sf::Vector2f mVelocity;
	sf::Vector2i mPickupPosition;

	float mMaxRotationSpeed; 
	float mWallRestitution;
	float mPlayerRestitution;

	bool mHasThrownGrenade;
	bool mIsLocal;

	uint32_t mPlayerId;
	string mPlayerName;
};

typedef shared_ptr<Player> PlayerPtr;
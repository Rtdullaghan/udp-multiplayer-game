#pragma once

namespace PlayerReplication
{
	enum State
	{
		Pos = 1 << 0,
		PlayerId = 1 << 1,
		Health = 1 << 2,
		Ammo = 1 << 3,
		Pickup = 1 << 4,
		Armour = 1 << 5,
		AllState = Pos | PlayerId | Health | Pickup | Armour
	};
}

namespace PickupReplication
{
	enum State
	{
		Pos = 1 << 0,
		Type = 1 << 1,
		AllState = Pos | Type
	};
}

namespace GrenadeReplication
{
	enum State {
		Pos = 1 << 0,
		PlayerId = 1 << 2,
		Exploding = 1 << 3,
		AllState = Pos | PlayerId | Exploding
	};
}

namespace ObstacleReplication
{
	enum State
	{
		Pos = 1 << 0,
		Type = 1 << 1,
		AllState = Pos | Type
	};
}

namespace BloodstainReplication
{
	enum State
	{
		Pos = 1 << 0,
		Type = 1 << 1,
		AllState = Pos | Type
	};
}
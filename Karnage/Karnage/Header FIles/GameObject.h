#define CLASS_IDENTIFICATION( inCode, inClass ) \
enum { kClassId = inCode }; \
virtual uint32_t GetClassId() const { return kClassId; } \
static GameObject* CreateInstance() { return static_cast< GameObject* >( new inClass() ); }

class GameObject : public sf::Transformable, public sf::Drawable {
public:
	CLASS_IDENTIFICATION('GOBJ', GameObject)

	GameObject();

	virtual ~GameObject() { }

	virtual Player* GetAsPlayer()
	{
		return nullptr;
	}

	virtual uint32_t GetAllStateMask() const
	{
		return 0;
	} 
	
	// Returns whether to keep processing collisions.
	virtual bool HandleCollisionWithPlayer(Player* inPlayer)
	{
		(void)inPlayer;
		return true;
	}

	virtual bool HandleCollisionWithGameObject(GameObject* inObj)
	{
		(void)inObj;
		return true;
	}

	virtual void Update();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	virtual void HandleDying() { }

	void SetIndexInWorld(int inIndex)
	{
		mIndexInWorld = inIndex;
	}

	int GetIndexInWorld() const
	{
		return mIndexInWorld;
	}

	float GetRotation() const
	{
		return mRotation;
	}

	void SetScale(float inScale)
	{
		mScale = inScale;
	}

	float GetScale() const
	{
		return mScale;
	}

	const sf::Vector2f& GetLocation() const
	{
		return mLocation;
	}

	void SetLocation(const sf::Vector2f& inLocation)
	{
		mLocation = inLocation;
	}

	float GetCollisionRadius() const
	{
		return mCollisionRadius;
	}

	void SetCollisionRadius(float inRadius)
	{
		mCollisionRadius = inRadius;
	}

	Vector3 GetForwardVector() const;

	void SetColor(const Vector3& inColor)
	{
		mColor = inColor;
	}

	const Vector3& GetColor() const
	{
		return mColor;
	}

	void SetObjectID(int objectID)
	{
		mObjectID = objectID;
	}

	int GetObjectID() const
	{
		return mObjectID;
	}

	bool DoesWantToDie() const
	{
		return mDoesWantToDie;
	}

	void SetDoesWantToDie(bool inWants)
	{
		mDoesWantToDie = inWants;
	}

	int GetNetworkId() const
	{
		return mNetworkId;
	}

	void SetNetworkId(int inNetworkId);
	void SetRotation(int inRotation);

	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
	{
		(void)inOutputStream;
		(void)inDirtyState;
		return 0;
	}

	virtual void Read(InputMemoryBitStream& inInputStream)
	{
		(void)inInputStream;
	}

private:
	sf::Vector2f mLocation;
	Vector3 mColor;
	float mCollisionRadius;
	float mRotation;
	float mScale;
	int mIndexInWorld;
	bool mDoesWantToDie;
	int mNetworkId;
	int mObjectID;
};

typedef shared_ptr<GameObject> GameObjectPtr;
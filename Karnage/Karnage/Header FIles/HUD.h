#pragma once

class HUD : public sf::Transformable, public sf::Drawable
{
public:
	HUD(Player* localPlayer);

	void SetPlayerName() const;

	void UpdatePlayerArrow(sf::Vector2f offset) const;
	void UpdateGrenadeAmmo();
	void UpdateHealthBar() const;
	void UpdateArmourBar() const;
	void UpdateDebugInfo(float rttMS, string bandwidth, int fps, int objectsDrawn);
	void UpdateScoreBoard();

	void UpdateScore();

	void setGameScreenBool(bool);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;


	//void DrawPlayerArrow(sf::RenderTarget& target, sf::RenderStates states) const;
	
private:
	void LoadComponants();
	void SetComponentPositions() const;
	void SetDebugInfo();
	void SetScoreAndScoreBoardInfo();

	
	
	void DrawGrenadeAmmo(sf::RenderTarget& target, sf::RenderStates states) const;
	void DrawScoreBoard(sf::RenderTarget& target, sf::RenderStates states) const;
	void DrawDebugInfo(sf::RenderTarget& target, sf::RenderStates states) const;
private:
	Player* mLocalPlayer;
	Powerbar* mHealthBar;
	Powerbar* mArmourBar;
	PlayerArrow* mPlayerArrow;

	sf::Sprite* mGrenadeSprite;
	sf::Font zombieFont;

	sf::Text RTT, InOut, drawnObjects, FPS, Score, scoreBoard;

	uint8_t mGrenadeAmmo;

	bool mDebug;
	bool mGameScreen;
};

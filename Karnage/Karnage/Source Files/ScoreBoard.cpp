#include <KarnagePCH.h>
ScoreBoard::ScoreBoard()
{
	SetScoreBoardInfo();
}

void ScoreBoard::UpdateScoreBoard()
{
	const vector< ScoreBoardManager::Entry > entries = ScoreBoardManager::sInstance->GetEntries();
	std::string scoreb = "";
	for (size_t i = 0; i < entries.size(); i++)
	{
		scoreb += entries[i].GetPlayerName() + StringUtils::Sprintf(" Score: %d\n", entries[i].GetScore());
	}
	scoreBoard.setString(scoreb);
}

void ScoreBoard::SetScoreBoardInfo()
{
	if (!zombieFont.loadFromFile("../Assets/Fonts/Exquisite Corpse.ttf"))
		LOG("ERROR FONT NOT LOADED IN HUD", 0);

	scoreBoard.setPosition(400, 20);

	scoreBoard.setFont(zombieFont);
	
	scoreBoard.setCharacterSize(Constant::Font::FONT_MEDIUM);
	
	scoreBoard.setFillColor(sf::Color::Black);
	scoreBoard.setOutlineThickness(0.5f);
	scoreBoard.setOutlineColor(sf::Color::Red);
}

void ScoreBoard::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
		DrawScoreBoard(target, states);
}
void ScoreBoard::DrawScoreBoard(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(scoreBoard, states);
}

#include <KarnagePCH.h>


PlayerArrow::PlayerArrow() :
	mArrow(10, 3)
	, mPlayerName("Player")
{
	mArrow.rotate(60);
	mArrow.setFillColor(sf::Color::Green);
	
	if (!zombieFont.loadFromFile("../Assets/Fonts/Exquisite Corpse.ttf"))
		LOG("ERROR FONT NOT LOADED IN PLAYER ARROW", 0);

	SetText();
}

void PlayerArrow::SetText()
{
	mPlayerNameText.setFont(zombieFont);
	mPlayerNameText.setCharacterSize(Constant::Font::FONT_MEDIUM);

	mPlayerNameText.setFillColor(sf::Color::Black);
	mPlayerNameText.setOutlineThickness(0.5f);
	mPlayerNameText.setOutlineColor(sf::Color::Red);

	mPlayerNameText.setString(mPlayerName);
}

void PlayerArrow::setColor(sf::Color color)
{
	mPlayerNameText.setOutlineColor(color);
	mArrow.setFillColor(color);
}

void PlayerArrow::setPosition(sf::Vector2f pos)
{
	mPlayerNameText.setPosition(sf::Vector2f(pos.x + 15 , pos.y));
	mArrow.setPosition(pos);
}

void PlayerArrow::setName(string name)
{
	mPlayerName = name;
	mPlayerNameText.setString(mPlayerName);
}


void PlayerArrow::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mPlayerNameText, states);
}

#include <KarnagePCH.h>

Grenade::Grenade() :
	mVelocity(sf::Vector2f(0, 0))
	, mThrowSpeed(400.f)
	, mPlayerId(0)
	, mIsExploding(false)
{
	SetScale(GetScale() * .05f);
	SetCollisionRadius(40.f);
}

void Grenade::SetThrowSpeed(float speed)
{
	mThrowSpeed = speed;
}

bool Grenade::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer; 
	return false;
}

bool Grenade::HandleCollisionWithGameObject(GameObject* inObj)
{
	(void)inObj;
	return false;
}

void Grenade::InitFromPlayer(Player* inPlayer, int variation)
{
	SetPlayerId(inPlayer->GetPlayerId());

	sf::Vector2f vel = inPlayer->GetVelocity();

	sf::Vector2f temp = sf::Vector2f(0, -1);
	thor::rotate(temp, inPlayer->GetRotation() + variation); 

	SetVelocity(sf::Vector2f(temp.x, temp.y) * mThrowSpeed);
	SetLocation(inPlayer->GetLocation());
	SetRotation(inPlayer->GetRotation());
}

void Grenade::Update()
{

}

void Grenade::HandleLocation()
{
	float deltaTime = Timing::sInstance.GetDeltaTime();

	SetVelocity(Utility::MoveTowards(GetVelocity(), sf::Vector2f(0.f, 0.f), 0.1f));

	SetLocation(GetLocation() + mVelocity * deltaTime);
}

#pragma region Networking

uint32_t Grenade::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	WritePosition(inOutputStream, inDirtyState, writtenState);

	WritePlayerID(inOutputStream, inDirtyState, writtenState);

	if (inDirtyState & GrenadeReplication::State::Exploding)
	{
		inOutputStream.Write((bool)mIsExploding);
		writtenState |= GrenadeReplication::State::Exploding;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

void Grenade::WritePosition(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & GrenadeReplication::State::Pos)
	{
		inOutputStream.Write((bool)true);
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		sf::Vector2f velocity = GetVelocity();
		inOutputStream.Write(velocity.x);
		inOutputStream.Write(velocity.y);
		inOutputStream.Write(GetRotation());
		writtenState |= GrenadeReplication::State::Pos;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
}

void Grenade::WritePlayerID(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{

	if (inDirtyState & GrenadeReplication::State::PlayerId)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(mPlayerId, Constant::Networking::PLAYER_ID_BITS);
		writtenState |= GrenadeReplication::State::PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
}
#pragma endregion
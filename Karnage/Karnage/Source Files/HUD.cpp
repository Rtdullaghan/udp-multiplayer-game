#include <KarnagePCH.h>

HUD::HUD(Player* localPlayer) :
	mLocalPlayer(localPlayer)
	, mHealthBar(new Powerbar(sf::Vector2f(Constant::Player::MAX_HEALTH, 20)))
	, mArmourBar(new Powerbar(sf::Vector2f(Constant::Player::MAX_ARMOUR, 20)))
	, mGrenadeAmmo(3)
	, mPlayerArrow(new PlayerArrow())
	, mDebug(false)
	, mGameScreen(true)
	, mGrenadeSprite(new sf::Sprite())
{
	LoadComponants();
	SetComponentPositions();
	SetScoreAndScoreBoardInfo();

	if (mDebug)
		SetDebugInfo();
}

#pragma region Initilization

void HUD::LoadComponants()
{
	// I started to move dependancys around so I could access TextureManager etc.. here
	// But this quickly became a mess, So For now I will Load like this

	if (!zombieFont.loadFromFile("../Assets/Fonts/Exquisite Corpse.ttf"))
		LOG("ERROR FONT NOT LOADED IN HUD", 0);

	sf::Texture* grenadeTexture(new sf::Texture());
	if (!grenadeTexture->loadFromFile("../Assets/Textures/Grenade.png"))
		LOG("ERROR TEXTURE NOT LOADED IN HUD", 0);

	mGrenadeSprite->setTexture(*grenadeTexture);

	mPlayerArrow->setColor(sf::Color::Green);
}

void HUD::SetComponentPositions() const
{
	mHealthBar->setPosition(
		sf::Vector2f(mHealthBar->GetOutlineThinkness(),
		(Constant::Window::VIEW_HEIGHT) - mHealthBar->GetDimensions().y - mHealthBar->GetOutlineThinkness()));
	mArmourBar->setPosition(
		sf::Vector2f(mArmourBar->GetOutlineThinkness(),
		(Constant::Window::VIEW_HEIGHT) - mArmourBar->GetDimensions().y - mArmourBar->GetOutlineThinkness()));
	// shouldn't be here as they're not positions
	mArmourBar->setColor(sf::Color::Cyan);
	mArmourBar->setOutlineThickness(0);

	mGrenadeSprite->setPosition(sf::Vector2f(Constant::Window::VIEW_WIDTH - Constant::HUD::Grenade::GRENADE_OFFSET, (Constant::Window::VIEW_HEIGHT) - Constant::HUD::Grenade::GRENADE_OFFSET));
	mGrenadeSprite->setScale(Constant::HUD::Grenade::GRENADE_SCALE, Constant::HUD::Grenade::GRENADE_SCALE);
}

void HUD::SetDebugInfo()
{
	RTT.setPosition(Constant::HUD::Debug::Positions::RTT_POSITION);
	InOut.setPosition(Constant::HUD::Debug::Positions::INOUT_POSITION);
	drawnObjects.setPosition(Constant::HUD::Debug::Positions::DRAWN_OBJECTS_POSITION);
	scoreBoard.setPosition(Constant::HUD::Debug::Positions::SCOREBOARD_POSITION);
	FPS.setPosition(Constant::HUD::Debug::Positions::FPS_POSITION);
	
	RTT.setFont(zombieFont);
	InOut.setFont(zombieFont);
	drawnObjects.setFont(zombieFont);
	scoreBoard.setFont(zombieFont);
	FPS.setFont(zombieFont);

	RTT.setCharacterSize(Constant::Font::FONT_MEDIUM);
	InOut.setCharacterSize(Constant::Font::FONT_MEDIUM);
	drawnObjects.setCharacterSize(Constant::Font::FONT_MEDIUM);
	scoreBoard.setCharacterSize(Constant::Font::FONT_MEDIUM);
	FPS.setCharacterSize(Constant::Font::FONT_MEDIUM);

	RTT.setFillColor(Constant::HUD::Debug::Colors::RTT_FILL_COLOR);
	RTT.setOutlineThickness(Constant::HUD::Debug::OUTLINE_THICKNESS);
	RTT.setOutlineColor(Constant::HUD::Debug::Colors::RTT_OUTLINE_COLOR);

	InOut.setFillColor(Constant::HUD::Debug::Colors::INOUT_FILL_COLOR);
	InOut.setOutlineThickness(Constant::HUD::Debug::OUTLINE_THICKNESS);
	InOut.setOutlineColor(Constant::HUD::Debug::Colors::INOUT_OUTLINE_COLOR);

	drawnObjects.setFillColor(Constant::HUD::Debug::Colors::D_OBJECTS_FILL_COLOR);
	drawnObjects.setOutlineThickness(Constant::HUD::Debug::OUTLINE_THICKNESS);
	drawnObjects.setOutlineColor(Constant::HUD::Debug::Colors::D_OBJECTS_OUTLINE_COLOR);

	FPS.setFillColor(Constant::HUD::Debug::Colors::FPS_FILL_COLOR);
	FPS.setOutlineThickness(Constant::HUD::Debug::OUTLINE_THICKNESS);
	FPS.setOutlineColor(Constant::HUD::Debug::Colors::FPS_OUTLINE_COLOR);

	scoreBoard.setFillColor(Constant::HUD::Debug::Colors::SCOREBOARD_FILL_COLOR);
	scoreBoard.setOutlineThickness(Constant::HUD::Debug::OUTLINE_THICKNESS);
	scoreBoard.setOutlineColor(Constant::HUD::Debug::Colors::SCOREBOARD_OUTLINE_COLOR);

}

void HUD::SetScoreAndScoreBoardInfo()
{
	Score.setPosition(Constant::Score::SCORE_POSITION);
	Score.setFont(zombieFont);
	Score.setCharacterSize(Constant::Font::FONT_LARGE);
	Score.setFillColor(Constant::Score::SCORE_FILL_COLOR);
	Score.setOutlineColor(Constant::Score::SCORE_OUTLINE_COLOR);
	Score.setOutlineThickness(Constant::Score::SCORE_OUTLINE_THICKNESS);

	scoreBoard.setPosition(Constant::Score::SCOREBOARD_POSITION);
	scoreBoard.setFont(zombieFont);
	scoreBoard.setCharacterSize(Constant::Font::FONT_LARGE);
	scoreBoard.setFillColor(Constant::Score::SCOREBOARD_FILL_COLOR);
	scoreBoard.setOutlineColor(Constant::Score::SCOREBOARD_OUTLINE_COLOR);
	scoreBoard.setOutlineThickness(Constant::Score::SCOREBOARD_OUTLINE_THICKNESS);
}

void HUD::SetPlayerName() const
{
	mPlayerArrow->setName(mLocalPlayer->GetPlayerName());
}

#pragma endregion

#pragma region Update
void HUD::UpdatePlayerArrow(sf::Vector2f offset) const
{
	mPlayerArrow->setPosition(Constant::Player::PLAYER_ARROW_POSITION + offset);
}

void HUD::UpdateScore()
{
	Score.setString(StringUtils::Sprintf("Score: %d", ScoreBoardManager::sInstance->GetEntry(mLocalPlayer->GetPlayerId())->GetScore()));
}

void HUD::UpdateGrenadeAmmo()
{
	mGrenadeAmmo = mLocalPlayer->GetGrenadeAmmo();
}

void HUD::UpdateHealthBar() const
{
	uint8_t health = mLocalPlayer->GetHealth();

	mHealthBar->setPower(health);

	if (health <= Constant::HUD::Player::HEALTH_BAR_LOW_THRESHOLD) {
		mHealthBar->setColor(Constant::HUD::Player::HEALTH_BAR_LOW_COLOR);
	}
	else if (health <= Constant::HUD::Player::HEALTH_BAR_MEDIUM_THRESHOLD) {
		mHealthBar->setColor(Constant::HUD::Player::HEALTH_BAR_MEDIUM_COLOR);
	}
	else {
		mHealthBar->setColor(Constant::HUD::Player::HEALTH_BAR_HIGH_COLOR);
	}
}

void HUD::UpdateArmourBar() const
{
	uint8_t armour = mLocalPlayer->GetArmour();

	mArmourBar->setPower(armour);
}
void HUD::UpdateScoreBoard()
{
	const vector< ScoreBoardManager::Entry > entries = ScoreBoardManager::sInstance->GetEntries();
	std::string scoreb = "";

	for (size_t i = 0; i < entries.size(); i++)
	{
		scoreb += entries[i].GetPlayerName() + StringUtils::Sprintf(" Score: %d\n", entries[i].GetScore()) ;
	}
	scoreBoard.setString(scoreb);
}

void HUD::UpdateDebugInfo(float rttMS, string bandwidth, int objectsDrawn, int fps)
{
	RTT.setString(StringUtils::Sprintf("RTT %d ms", static_cast<int>(rttMS))); 
	InOut.setString(bandwidth);

	FPS.setString(StringUtils::Sprintf("FPS %d", fps));
	drawnObjects.setString(StringUtils::Sprintf("FPS %d", objectsDrawn));
}

void HUD::setGameScreenBool(bool gameScreen)
{
	mGameScreen = gameScreen;
}
#pragma endregion

#pragma region Draw

void HUD::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(Score, states);
	target.draw(*mPlayerArrow, states);
	target.draw(*mHealthBar, states);
	target.draw(*mArmourBar, states);
	DrawGrenadeAmmo(target, states);
	DrawScoreBoard(target, states);

	if (mDebug)
		DrawDebugInfo(target, states);
}

void HUD::DrawGrenadeAmmo(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (size_t i = 1; i <= mGrenadeAmmo; i++)
	{
		target.draw(*mGrenadeSprite, states);
		mGrenadeSprite->setPosition(Constant::Window::VIEW_WIDTH - (Constant::HUD::Grenade::GRENADE_OFFSET * i), (Constant::Window::VIEW_HEIGHT) - Constant::HUD::Grenade::GRENADE_OFFSET);
	}
	
}

void HUD::DrawDebugInfo(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(RTT, states);
	target.draw(InOut, states);

	target.draw(FPS, states);
	target.draw(drawnObjects, states);
}

void HUD::DrawScoreBoard(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(scoreBoard, states);
}
#pragma endregion
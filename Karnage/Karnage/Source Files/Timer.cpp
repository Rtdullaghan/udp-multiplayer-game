#include "KarnagePCH.h"

Timer::Timer(float finishTime) :
	mFinishTime(finishTime)
	, mCurrentTime(0)
	, mTimerStarted(false)
	, mTimerFinished(false){}


void Timer::Reset()
{
	mCurrentTime = 0;
	mTimerStarted = false;
	mTimerFinished = false;
}

void Timer::Update(float deltaTime)
{
	if (mTimerStarted)
	{
		Tick(deltaTime);
		CheckFinished();
	}
}

void Timer::Tick(float deltaTime)
{
	mCurrentTime += deltaTime;
}

void Timer::CheckFinished()
{
	if (mCurrentTime >= mFinishTime)
	{
		mTimerFinished = true;
		mTimerStarted= false;
	}
}

void Timer::Start()
{
	mTimerStarted = true;
}

bool Timer::IsStarted() const
{
	return mTimerStarted;
}

bool Timer::IsFinished() const
{
	return mTimerFinished;
}

float Timer::GetElapsedSeconds() const
{
	return mCurrentTime * 1000.f;
}

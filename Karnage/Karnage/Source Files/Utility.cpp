#include<KarnagePCH.h>




namespace Utility
{

#pragma region centerOrigin

	void centreOrigin(sf::RectangleShape shape)
	{
		sf::FloatRect bounds = shape.getLocalBounds();
		shape.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}

	void centerOrigin(sf::Sprite & sprite)
	{
		sf::FloatRect bounds = sprite.getLocalBounds();
		sprite.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}

	void centreOrigin(sf::Sprite & sprite)
	{
		sf::FloatRect bounds = sprite.getLocalBounds();
		sprite.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}

	void centerOrigin(sf::Text & text)
	{
		sf::FloatRect bounds = text.getLocalBounds();
		text.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}

	void centerOrigin(Animation & animation)
	{
		sf::FloatRect bounds = animation.getLocalBounds();
		animation.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}

	void centreOrigin(Animation & animation)
	{
		sf::FloatRect bounds = animation.getLocalBounds();
		animation.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
	}
#pragma endregion

#pragma region Conversions
	float toRadians(float degrees)
	{
		return degrees * M_PI / 180.f;
	}

	float toDegrees(float radians)
	{
		return radians * 180.f / M_PI;
	}
#pragma endregion

#pragma region Vectors

	sf::Vector2f unitVector(sf::Vector2f vector)
	{
		assert(vector != sf::Vector2f(0.f, 0.f));
		return vector / length(vector);
	}

	sf::Vector2f normalize(const sf::Vector2f & source)
	{
		float length = sqrt(source.x * source.x + source.y * source.y);
		if (length != 0)
			return sf::Vector2f(source.x / length, source.y / length);
		return source;
	}

	float length(sf::Vector2f vector)
	{
		return std::sqrt(vector.x * vector.x + vector.y * vector.y);
	}

	float vectorDistance(sf::Vector2f v1, sf::Vector2f v2)
	{
		sf::Vector2f dV = v1 - v2;
		return length(dV);
	}

	float dot(const sf::Vector2f& inLeft, const sf::Vector2f& inRight)
	{
		return (inLeft.x * inRight.x + inLeft.y * inRight.y);
	}

	float NormalizedEuclideanDistance(sf::Vector2f v1, sf::Vector2f v2)
	{

		float distance = sqrt(((v1.x - v2.x)/2) + ((v1.y - v2.y)/2));
			return distance;;
	}

	sf::Vector2f lerp(const sf::Vector2f& inA, const sf::Vector2f& inB, float t)
	{
		return sf::Vector2f(inA + t * (inB - inA));
	}

#pragma endregion

#pragma region Math

	void clamp(float& value, float min, float max)
	{
		value = std::max(min, std::min(value, max));
	}

	void clamp(int& value, int min, int max)
	{
		value = std::max(min, std::min(value, max));
	}

	sf::Vector2f MoveTowards(sf::Vector2f current, sf::Vector2f target, float maxDelta)
	{
		sf::Vector2f a = target - current;

		float magnitude = length(a);

		if (magnitude <= maxDelta || magnitude == 0.f)
		{
			return target;
		}
		return current + a / magnitude * maxDelta;
	}

	bool inRange(unsigned low, unsigned high, unsigned x)
	{
		return  ((x - low) <= (high - low));
	}


#pragma endregion

#pragma region Random

	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();

	int randomInt(int exclusiveMax)
	{
		std::uniform_int_distribution<> distr(0, exclusiveMax - 1);
		return distr(RandomEngine);
	}

	int randomRange(int range)
	{
		int rand = randomInt(range);
		if (rand > (range / 2))
			rand = (rand - (range / 2)) * -1;

		return rand;
	}

	sf::Vector2i GetRandomVector(const sf::Vector2i& inMax)
	{
		return sf::Vector2i(randomInt(inMax.x), randomInt(inMax.y));
	}


#pragma endregion

}



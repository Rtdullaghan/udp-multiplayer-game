#include <KarnagePCH.h>

Bloodstain::Bloodstain()
{
	SetScale(GetScale() * .6f);
	SetCollisionRadius(45.f);
}


void Bloodstain::SetBloodstainType(uint8_t type)
{
	mBloodstainType = static_cast<Enum::BloodstainType>(type);
}

void Bloodstain::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}
bool Bloodstain::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer;
	return false;
}

bool Bloodstain::HandleCollisionWithGameObject(GameObject* inObj)
{
	(void)inObj;
	return false;
}

#pragma region Networking

uint32_t Bloodstain::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	WriteBloodstainPosition(inOutputStream, writtenState, inDirtyState);
	WriteBloodstainType(inOutputStream, writtenState, inDirtyState);

	return writtenState;
}

void Bloodstain::WriteBloodstainPosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & BloodstainReplication::State::Pos)
	{
		inOutputStream.Write(static_cast<bool>(true));
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		inOutputStream.Write(GetRotation());
		writtenState |= BloodstainReplication::State::Pos;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Bloodstain::WriteBloodstainType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & BloodstainReplication::State::Type)
	{
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(static_cast<uint8_t>(mBloodstainType), Constant::Networking::BLOODSTAIN_TYPE_BITS); //TODO Smaller Data Type 
		writtenState |= BloodstainReplication::State::Type;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

#pragma endregion

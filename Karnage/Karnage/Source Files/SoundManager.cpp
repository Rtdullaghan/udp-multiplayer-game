#include "KarnagePCH.h"
std::unique_ptr<SoundManager> SoundManager::sInstance;

void SoundManager::StaticInit()
{
	sInstance.reset(new SoundManager());
}

SoundManager::SoundManager()
{
	LoadSoundFromFile(health, healthB, "../Assets/audio/Health.wav");
	LoadSoundFromFile(cloak, cloakB, "../Assets/audio/Cloak.wav");
	LoadSoundFromFile(fire_rate, fire_rateB, "../Assets/audio/Fire_Rate.wav");
	LoadSoundFromFile(shotgun, shotgunB, "../Assets/audio/PumpShotgun.wav");
	LoadSoundFromFile(armour, armourB, "../Assets/audio/ZipArmour.wav");
	LoadSoundFromFile(ammo, ammoB, "../Assets/audio/Ammo.wav");
	LoadSoundFromFile(shoot, shootB, "../Assets/audio/hit.wav", true);
	LoadSoundFromFile(death, deathB, "../Assets/audio/death.wav", true);
	LoadSoundFromFile(bark1, bark1B, "../Assets/audio/MarineBark1.wav");
	LoadSoundFromFile(bark2, bark2B, "../Assets/audio/MarineBark2.wav");
	LoadSoundFromFile(bark3, bark3B, "../Assets/audio/MarineBark3.wav");
	LoadSoundFromFile(explosion1, explosion1B, "../Assets/audio/Explosion1.wav", true);
	LoadSoundFromFile(explosion2, explosion2B, "../Assets/audio/Explosion2.wav", true);

	LoadMusicFromFile(bgMusic, "../Assets/audio/background.wav");
}

void SoundManager::LoadSoundFromFile(sf::Sound& p_sound, sf::SoundBuffer& p_buffer, string p_file, bool is3D)
{
	if(p_buffer.loadFromFile(p_file))
	{
		p_sound.setBuffer(p_buffer);
		if (is3D)
			p_sound.setVolume(85);
		else
			p_sound.setVolume(35);
	}
}

void SoundManager::LoadMusicFromFile(sf::Music& p_music, string p_file)
{
	p_music.openFromFile(p_file);
	p_music.setLoop(true);
	p_music.setVolume(10);
}

void SoundManager::PlayMusic()
{
	bgMusic.play();
}

void SoundManager::PlaySound(Enum::Sound p_sound)
{
	switch(p_sound)
	{
	case Enum::Sound::P_HEALTH:
		health.setRelativeToListener(true);
		health.play();
			break;
	case Enum::Sound::P_ARMOUR:
		armour.setRelativeToListener(true);
		armour.play();
		break;
	case Enum::Sound::P_AMMO:
		ammo.setRelativeToListener(true);
		ammo.play();
		break;

	case Enum::Sound::P_CLOAK:
		cloak.setRelativeToListener(true);
		cloak.play();
		break;

	case Enum::Sound::P_FIRE_RATE:
		fire_rate.setRelativeToListener(true);
		fire_rate.play();
		break;
	case Enum::Sound::P_SHOTGUN:
		shotgun.setRelativeToListener(true);
		shotgun.play();
		break;

		case Enum::Sound::SHOOT:
			shoot.setRelativeToListener(true);
			shoot.setVolume(Constant::Sound::SHOOT_VOLUME);
			shoot.play();
			break;
		case Enum::Sound::DEATH:
			death.setRelativeToListener(true);
			death.play();
			break;
		case Enum::Sound::JOIN:
			PlayJoinSound();
			break;
	}
}

void SoundManager::PlayJoinSound()
{
	//Note the random sound played might be differnt on diffent clients, But who cares!
	int rand = Utility::randomInt(3);
	if (rand == 0)
	{
		bark1.setRelativeToListener(true);
		bark1.play();
	}
	else if(rand == 1)
	{
		bark2.setRelativeToListener(true);
		bark2.play();
	}
	else
	{
		bark3.setRelativeToListener(true);
		bark3.play();
	}
}

void SoundManager::PlaySoundAtLocation(Enum::Sound p_sound, sf::Vector2f p_location)
{
	//TODO Make This Better

	LOG("SOUND LOCATION X: %d Y: %d", p_location.x, p_location.y);
	switch(p_sound)
	{
		case Enum::Sound::SHOOT:
			shoot.setAttenuation(0.15f);
			shoot.setVolume(Constant::Sound::SHOOT_VOLUME);
			shoot.setPosition(p_location.x, p_location.y, 0);
			shoot.play();
			break;

		case Enum::Sound::DEATH:
			death.setAttenuation(0.15f);
			death.setPosition(p_location.x, p_location.y, 0);
			death.play();
			break;

		case Enum::Sound::EXPLOSION:
			//TODO Refactor
			if (Utility::randomInt(2) == 0)
			{
				explosion1.setAttenuation(0.005f);
				explosion1.setPosition(p_location.x, p_location.y, 0);
				explosion1.play();
			}
			else
			{
				explosion2.setAttenuation(0.005f);
				explosion2.setPosition(p_location.x, p_location.y, 0);
				explosion2.play();
			}

		default:
			break;
	}
}
